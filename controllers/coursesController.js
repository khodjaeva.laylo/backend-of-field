const Course=require('../models/courses')
const fs = require('fs');
const path = require('path');
const sharp = require('sharp');
const multer = require('multer');
const UploadFile=require('../config/uploadFile')


exports.createCourse = async (req, res) => {
    const files = req.file;
        const {filename} = files;
    const thumb=(`/public/uploads/thumb/${filename}`)
    const SharpMetod=new UploadFile(filename)
    SharpMetod.sharpMetod()
    const course= new Course({
        name:req.body.name,
        description:req.body.description,
        image:thumb,
        category:req.body.category
   })
   course.save()
   .then(()=>{
       res.status(201).json({
            success:true,
            data:course
       })
   })
   .catch((error)=>{
       res.status(404).json({
            success:false,
            data:error
       })
   })
}


exports.getAllCourses=async(req,res)=>{
    const courses=await Course.find()
    res.status(200).json({
         success:true,
         data:courses
    })
}

exports.editCourse=async(req,res)=>{
    const course=await Course.findByIdAndUpdate(req.params.id)
    course.name=req.body.name,
    course.description=req.body.description,
    course.date=req.body.date,
    course.image=req.body.image
    course.save()
    .then(()=>{
        res.status(200).json({
             success:true,
             data:course
        })
   })
   .catch((error)=>{
       res.status(404).json({
             success:false,
             data:error
       })
   })
}

exports.deleteCourse=async (req,res)=>{
    await Course.findById(req.params.id)
        .exec((error,data)=>{
            if(error){res.send(error)}
            else{
                const filePath=path.join(path.dirname(__dirname)+data.image)
                console.log(path.join(path.dirname(__dirname)+data.image))
                fs.unlink(filePath, async (err)=>{
                    if(err) throw err
                    await Course.findByIdAndDelete(req.params.id)
                    res.status(200).json({
                        success:true,
                        data:"Success delete"
                    })
                })
            }
        })
}
