const express = require('express');
const router= express.Router()
const coursesController=require('../controllers/coursesController')
const path=require('path')
const multer = require('multer');
const md5 = require('md5');

const storage=multer.diskStorage({
    destination:function(req, file, cb){
        cb(null, './public/uploads/org');
    },
    filename:function(req,file,cb){
        cb(null,`${md5(Date.now())}${path.extname(file.originalname)}`);
    }
})

const upload= multer({storage:storage})

router.post('/add',upload.single('image'),coursesController.createCourse),
router.get('/all',coursesController.getAllCourses)
router.put('/:id',coursesController.editCourse)
router.delete('/:id',coursesController.deleteCourse)

module.exports=router;