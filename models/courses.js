const mongoose=require('mongoose')

const CourseSchema= mongoose.Schema({
    name:{type:String,required:true},
    description:{type:String,required:true},
    date:{type:Date,default:Date.now()},
    image:{type:String,required:true},
    category: {
        type:mongoose.Schema.ObjectId,
        ref: 'Category',
        required: true
    }
})

module.exports=mongoose.model('Course',CourseSchema)