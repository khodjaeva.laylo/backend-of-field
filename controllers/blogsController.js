const Blog = require('../models/blogs');
const fs = require('fs');
const path = require('path');
const sharp = require('sharp');
const multer = require('multer');
const UploadFile=require('../config/uploadFile')

exports.addBlog=(req,res)=>{
    const files = req.file;
        const {filename} = files;
    const thumb=(`/public/uploads/thumb/${filename}`)
    const SharpMetod=new UploadFile(filename)
    SharpMetod.sharpMetod()
    const blog=new Blog ({
         name:req.body.name,
         date:req.body.date,
         description:req.body.description,
         image:thumb
    })
    blog.save()
    .then(()=>{
        res.status(201).json({
             success:true,
             data:blog
        })
    })
    .catch((error)=>{
        res.status(404).json({
             success:false,
             data:error
        })
    })
}

exports.getAllBlogs=async(req,res)=>{
    const blogs=await Blog.find()
    res.status(201).json({
         success:true,
         data:blogs
    })
}

exports.updateBlog=async(req,res)=>{
    const blog=await Blog.findByIdAndUpdate(req.params.id)
    blog.name=req.body.name,
    blog.description=req.body.description,
    blog.date=req.body.date,
    blog.image=req.body.image
    blog.save()
    .then(()=>{
    res.status(201).json({
         success:true,
         data:blog
      })
    })
    .catch((error)=>{
        res.status(404).json({
             success:false,
             data:error
        })
    })

}

exports.deleteBlog=async (req,res)=>{
    await Blog.findById(req.params.id)
        .exec((error,data)=>{
            if(error){res.send(error)}
            else{
                const filePath=path.join(path.dirname(__dirname)+data.image)
                console.log(path.join(path.dirname(__dirname)+data.image))
                fs.unlink(filePath, async (err)=>{
                    if(err) throw err
                    await Blog.findByIdAndDelete(req.params.id)
                    res.status(200).json({
                        success:true,
                        data:"Success delete"
                    })
                })
            }
        })
}
