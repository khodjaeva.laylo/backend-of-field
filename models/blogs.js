const mongoose=require('mongoose')

const BlogSchema= mongoose.Schema({
    name:{type:String,required:true},
    description:{type:String,required:true},
    date:{type:Date,default:Date.now()},
    image:{type:String,required:true}
})

module.exports=mongoose.model('Blog',BlogSchema)