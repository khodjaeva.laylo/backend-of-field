const express= require('express');
const bodyParser=require('body-parser');
const cors =require('cors');
const config = require ('./config/mongoDB');
config()
const path=require('path').join(__dirname, '/public/uploads')

const app= express()

app.use(bodyParser.json())
app.use(express.static('public/uploads'))
app.use('/uploads', express.static(__dirname + '/public/uploads'))
app.use('/category',require('./routes/categoryRouter'),express.static(__dirname + '/public/uploads'))
app.use('/blogs',require('./routes/blogsRouter'))
app.use('/courses',require('./routes/coursesRouter'))
app.use('/poster',require('./routes/postersRouter'))



const Port=5000;
app.listen(Port,()=>{
    console.log('Server is running on Port '+ Port)
})