const express = require('express');
const router= express.Router()
const postersController=require('../controllers/postersController')
const path = require('path');
const multer = require('multer');
const md5 = require('md5');

const storage=multer.diskStorage({
	destination:function(req,file,cb){
		cb(null, './public/uploads/org')
	},
	filename:function(req,file,cb){
		cb(null, `${md5(Date.now())}${path.extname(file.originalname)}`);
	}
})

const upload =multer({storage:storage})

router.post('/add',upload.single('image'),postersController.addPoster),
router.get('/all',postersController.getAllPosters)
router.put('/:id',postersController.editPoster)
router.delete('/:id',postersController.deletePoster)

module.exports=router;