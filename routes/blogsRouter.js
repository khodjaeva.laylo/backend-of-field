const express= require('express')
const router=express.Router()
const blogsController = require('../controllers/blogsController')
const path = require('path');
const multer = require('multer');
const md5 = require('md5');

const storage = multer.diskStorage({
    destination:function(req, file, cb){
        cb(null, './public/uploads/org');},
        filename:function(req,file,cb){
            cb(null,`${md5(Date.now())}${path.extname(file.originalname)}`);

        }
    
})

const upload = multer({storage:storage})

router.post('/add',upload.single('image'),blogsController.addBlog)
router.get('/all',blogsController.getAllBlogs)
router.put('/:id',blogsController.updateBlog)
router.delete('/:id',blogsController.deleteBlog)


module.exports=router;