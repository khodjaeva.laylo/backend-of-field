const Category = require('../models/category');

exports.addCategory=(req,res)=>{
    const category=new Category({
        name:req.body.name,
        date:req.body.date
    })
    category.save()
    .then(()=>{
        res.status(201).json({
            success:true,
            date:category
        })
    })
    .catch((error)=>{
        res.status(404).json({
            success:false,
            data:error
        })
    })
}

exports.getAllCategories=async(req,res)=>{
    const categories=await Category.find()
    res.status(200).json({
        success:true,
        data:categories
    })
}

exports.deleteCategory=async(req,res)=>{
    await Category.findByIdAndDelete(req.params.id)
    res.status(200).json({
        success:true,
        data:[]
    })
}

exports.editCategory=async(req,res)=>{
    const category= await Category.findByIdAndUpdate(req.params.id)
    category.name=req.body.name,
    category.date=req.body.date
    category.save()
    .then(()=>{
        res.status(201).json({
            success:true,
            data:category
        })
    })
    .catch((error)=>{
        res.status(404).json({
            success:false,
            data:error
        })
    })
}