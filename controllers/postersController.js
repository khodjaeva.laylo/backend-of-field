const UploadFile = require('../config/uploadFile');
const Poster = require('../models/posters');
const fs = require('fs');
const sharp = require('sharp');
const md5 = require('md5');
const multer = require('multer');

exports.addPoster=(req,res)=>{
const files = req.file;
const {filename} = files;
const thumb= (`/public/uploads/thumb/${filename}`)
const sharpMetod = new UploadFile(filename)
sharpMetod.sharpMetod()

    const poster=new Poster({
        name:req.body.name,
        date:req.body.date,
        description:req.body.description,
        image:thumb
    })
    poster.save()
    .then(()=>{
        res.status(201).json({
            success:true,
            date:poster
        })
    })
    .catch((error)=>{
        res.status(404).json({
            success:false,
            data:error
        })
    })
}

exports.editPoster=async(req,res)=>{
    const poster= await Poster.findByIdAndUpdate(req.params.id)
    poster.name=req.body.name,
    poster.date=req.body.date,
    poster.description=req.body.description,
    poster.image=req.body.image
    poster.save()
    .then(()=>{
        res.status(201).json({
            success:true,
            data:poster
        })
    })
    .catch((error)=>{
        res.status(400).json({
            success:false,
            data:error
        })
    })
}

exports.getAllPosters=async(req,res)=>{
    const posters=await poster.find()
    res.status(200).json({
        success:true,
        data:posters
    })
}

exports.deletePoster=async(req,res)=>{
    await Poster.findById(req.params.id)
    .exec((error,data)=>{
        if(error) throw error
        else{const filePath= path.join(path.dirname(__dirname)+data.image)
        console.log(path.join(path.dirname)+data.image)
        fs.unlink(filePath , async(err)=>{
            if(err) throw err
            await Poster.findByIdAndDelete(req.params.id)
            res.status(200).json({
                success:true,
                data:"Successfully deleted"
            })
        })
        }
       
    })
}

